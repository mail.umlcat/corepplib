/** Module: "core"
 ** Filename: "core.c"
 ** Qualified Identifier:
 ** "core" 
 ** Descr.:
 ** "Global (Root) Core Library Package Module"
 **/

// namespace core {
 
// ------------------

// ------------------

// --> this module own header goes here
#include "core.h"
 
// ------------------


 // ...

// ------------------

/* override */ int /* func */ core__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  //coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  //coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace core