/** Module: "coreobjs_base.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coreobjs_base
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coreobjsbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coreobjsmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coreobjschars.h"

// ------------------

// --> same package libraries here
// .include "coreobjs_basebinfloat16s.h"
// .include "coreobjs_basebinfloat32s.h"
// .include "coreobjs_basebinfloat64s.h"
// .include "coreobjs_basebinfloat128s.h"
// .include "coreobjs_basebinfloat256s.h"
// .include "coreobjs_baselongfloats.h"

// ------------------

// --> this same module header goes here
#include "coreobjs_base.h"
 

// ------------------

/* $$ internal */ extern struct coreobjs_base_objectclassvmt* /* $$ var */ coreobjs_base_objectclassvmt;

// ------------------

/* functions */

int /* $$ func */ coreobjs_base_objectclass_create
  ( pointer* /* $$ param */ self )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // Does Nothing in purpouse, not abstract
  
  // ---
  
  return ErrorCode;
} // func

int /* $$ func */ coreobjs_base_objectclass_destroy
  ( pointer* /* $$ param */ self )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // Does Nothing in purpouse, not abstract
  
  // ---
  
  return ErrorCode;
} // func

int /* $$ func */ coreobjs_base_objectclass_donothing
  ( pointer* /* $$ param */ self )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // Does Nothing in purpouse, not abstract
  
  // ---
  
  return ErrorCode;
} // func

// ------------------

 // ...
 
// ------------------

/* $$ internal */ int /* $$ func */ coreobjs_base_objectclass_initclass
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // allocate this class V.M.T.
  coreobjs_base_objectclassvmt =
    malloc(sizeof(struct coreobjs_base_objectclass));
	
  // ---
 
  // assign virtual methods
  coreobjs_base_objectclassvmt->create =
    /* $$ & */ coreobjs_base_objectclass_create;
  coreobjs_base_objectclassvmt->destroy =
    /* $$ & */ coreobjs_base_objectclass_destroy;
  coreobjs_base_objectclassvmt->donothing =
    /* $$ & */ coreobjs_base_objectclass_donothing;
  
  // ---
  
  return ErrorCode;
} // func

/* $$ internal */ int /* $$ func */ coreobjs_base_objectclass_doneclass
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // deallocate this class V.M.T.
  delete (coreobjs_base_objectclassvmt, sizeof(struct coreobjs_base_objectclass));
  
  // ---
  
  return ErrorCode;
} // func

// ---

/* $$ internal */ int /* $$ func */ coreobjs_base_objectclass_initclass
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // allocate this class V.M.T.
  coreobjs_base_objectclassvmt =
    malloc(sizeof(struct coreobjs_base_objectclass));
	
  // ---
 
  // assign virtual methods
  coreobjs_base_objectclassvmt->create =
    /* $$ & */ coreobjs_base_objectclass_create;
  coreobjs_base_objectclassvmt->destroy =
    /* $$ & */ coreobjs_base_objectclass_destroy;
  coreobjs_base_objectclassvmt->donothing =
    /* $$ & */ coreobjs_base_objectclass_donothing;
  
  // ---
  
  return ErrorCode;
} // func

/* $$ internal */ int /* $$ func */ coreobjs_base_objectclass_doneclass
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  // deallocate this class V.M.T.
  delete (coreobjs_base_objectclassvmt, sizeof(struct coreobjs_base_objectclass));
  
  // ---
  
  return ErrorCode;
} // func

/* $$ internal */ int /* $$ func */ coreobjs_base_initclasses
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---

  coreobjs_base_objectclass_initclass( noparams );
  
  // ---
  
  return ErrorCode;
} // func

/* $$ internal */ int /* $$ func */ coreobjs_base_doneclasses
  ( noparams )
{
  int /* $$ var */ ErrorCode = 0;
  
  // ---
  
  coreobjs_base_objectclass_doneclass( noparams );
  
  // ---
  
  return ErrorCode;
} // func




// ------------------

/* $$ override */ int /* $$ func */ coreobjs_base__start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---

  coreobjs_base_initclasses( noparams );
  //coreobjs_base_initintfs( noparams );
  
  //coreobjsbasefuncs__nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coreobjs_base__finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coreobjsbasefuncs__nothing();
  
  //coreobjs_base_doneintfs( noparams );
  coreobjs_base_doneclasses( noparams );
  
  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coreobjs_base
