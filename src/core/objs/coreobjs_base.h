/** Module: "coreobjs_base.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coreobjs_base
// $$ {
 
// ------------------
 
#ifndef CORESYSOBJS_BASE_H
#define CORESYSOBJS_BASE_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coreobjsbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coreobjsmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coreobjschars.h"

// ------------------

// --> same package libraries here
// .include "coreobjs_basebinfloat16s.h"
// .include "coreobjs_basebinfloat32s.h"
// .include "coreobjs_basebinfloat64s.h"
// .include "coreobjs_basebinfloat128s.h"
// .include "coreobjs_basebinfloat256s.h"
// .include "coreobjs_baselongfloats.h"

// ------------------

/* rtti */

#define MOD_coreobjs_base {0xC4,0x1E,0xC9,0xAD,0x79,0xA1,0x97,0x45,0xA9,0xE8,0xF4,0xC3,0xF0,0xE0,0xB7,0xC9};

// ------------------

/* types */

typedef
  int (* defaultctor) /* $$ functor */ ( pointer* /* $$ param */ self );

typedef
  int (* defaultdtor) /* $$ functor */ ( pointer* /* $$ param */ self );

typedef
  int (* nothingfctor) /* $$ functor */ ( pointer* /* $$ param */ self );

// ------------------

/* $$ protected */ struct coreobjs_base_objectclassprop
{
  int /* $$ var */ dummy;
} ;

/* $$ public */ struct coreobjs_base_objectclass
{
  // prev class fields
  // // none
  // this class fields
  struct coreobjs_base_objectclassprop /* $$ var */ AsObject;
} ;



// ---

/* $$ protected */ struct coreobjs_base_objectclassmet
{
  // /* $$ virtual */ int /* $$ func */ create
  //  ( pointer* /* $$ param */ self );
  defaultctor  /* $$ var */ create;
  
  // /* $$ virtual */ int /* $$ func */ destroy
  //  ( pointer* /* $$ param */ self );
  defaultdtor  /* $$ var */ destroy;
  
  // /* $$ virtual */ /* $$ sealed */ int /* $$ func */ nothing
  //  ( pointer* /* $$ param */ self );
  nothingfctor /* $$ var */ donothing;
} ;

/* $$ public */ struct coreobjs_base_objectclassvmt
{
  struct coreobjs_base_objectclassmet /* $$ var */ AsObject;
} ;

// ------------------

/* rtti */

#define MOD_coreobjs_base {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

/* rtti */

#define ID_coreobjs_base_objectclass {0x79,0x93,0xAE,0x91,0xFB,0x77,0x8F,0x43,0xA3,0xF3,0x98,0x39,0x96,0xFF,0xC3,0xE3};

// ------------------

/* $$ internal */ struct coreobjs_base_objectclassvmt* /* $$ var */ coreobjs_base_objectclassvmt;

// ------------------

/* functions */

int /* $$ func */ coreobjs_base_objectclass_create
  ( pointer* /* $$ param */ self );

int /* $$ func */ coreobjs_base_objectclass_destroy
  ( pointer* /* $$ param */ self );

int /* $$ func */ coreobjs_base_objectclass_donothing
  ( pointer* /* $$ param */ self );

// ------------------

 // ...
 
// ------------------

/* $$ internal */ int /* $$ func */ coreobjs_base_objectclass_initclass
  ( noparams );
  
// ---

// ------------------

/* $$ override */ int /* $$ func */ coreobjs_base__start
  ( noparams );

/* $$ override */ int /* $$ func */ coreobjs_base__finish
  ( noparams );

// ------------------

#endif // CORESYSOBJS_BASE_H

// $$ } // namespace coreobjs_base
