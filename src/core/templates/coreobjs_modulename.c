/** Module: "coreobjs<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coreobjs<modulename>s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coreobjsbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coreobjsmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coreobjschars.h"

// ------------------

// --> same package libraries here
// .include "coreobjs<modulename>sbinfloat16s.h"
// .include "coreobjs<modulename>sbinfloat32s.h"
// .include "coreobjs<modulename>sbinfloat64s.h"
// .include "coreobjs<modulename>sbinfloat128s.h"
// .include "coreobjs<modulename>sbinfloat256s.h"
// .include "coreobjs<modulename>slongfloats.h"

// ------------------

// --> this same module header goes here
#include "coreobjs<modulename>s.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coreobjs<modulename>s__start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coreobjsbasefuncs__nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coreobjs<modulename>s__finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coreobjsbasefuncs__nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coreobjs<modulename>s
