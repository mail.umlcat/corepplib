/** Module: "coreobjs<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coreobjs<modulename>s
// $$ {
 
// ------------------
 
#ifndef COREOBJS<MODULENAME>S_H
#define COREOBJS<MODULENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coreobjsbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coreobjsmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coreobjschars.h"

// ------------------

// --> same package libraries here
// .include "coreobjs<modulename>sbinfloat16s.h"
// .include "coreobjs<modulename>sbinfloat32s.h"
// .include "coreobjs<modulename>sbinfloat64s.h"
// .include "coreobjs<modulename>sbinfloat128s.h"
// .include "coreobjs<modulename>sbinfloat256s.h"
// .include "coreobjs<modulename>slongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coreobjs<modulename>s {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coreobjs<modulename>s__start
  ( noparams );

/* $$ override */ int /* $$ func */ coreobjs<modulename>s__finish
  ( noparams );

// ------------------

#endif // COREOBJS<MODULENAME>S_H

// $$ } // namespace coreobjs<modulename>s