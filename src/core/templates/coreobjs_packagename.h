/** Module: "coreobjs<packagename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coreobjs<packagename>s
// $$ {
 
// ------------------
 
#ifndef COREOBJS<PACKAGENAME>S_H
#define COREOBJS<PACKAGENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coreobjsbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coreobjsmemints.h"

// ------------------

// --> contained modules here
// .include "coreobjs<packagename>sbinfloat16s.h"
// .include "coreobjs<packagename>sbinfloat32s.h"
// .include "coreobjs<packagename>sbinfloat64s.h"
// .include "coreobjs<packagename>sbinfloat128s.h"
// .include "coreobjs<packagename>sbinfloat256s.h"
// .include "coreobjs<packagename>slongfloats.h"

// ------------------

/* rtti */

#define MOD_coreobjs<packagename>s {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coreobjs<packagename>s__start
  ( noparams );

/* $$ override */ int /* $$ func */ coreobjs<packagename>s__finish
  ( noparams );

// ------------------

#endif // COREOBJS<PACKAGENAME>S_H

// $$ } // namespace coreobjs<packagename>s
